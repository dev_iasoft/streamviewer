# Stream Viewer


## How to use


- Login using OAuth
- Select a video from list
- click on chat tab, and "init" button (to start connection)
- Write something and hit enter


Open a new browser
- Login using OAuth
- Select the same video from previous step
- click on chat tab, and "init" button (to start connection)
- Write something and hit enter

Back to first browser
- you'll see the message sent from other user here

- If you click on video on list, and open the tab "History messages", you'll be able to see the history of messages. On this tab you can filter the messages per user.
- And opening the "Fans List" you'll see the number of messages per user, for this video.


## How was made

This project is structured in 3 main containers:

1. Application
2. Database
3. Chat server


### Application
The application is divided in UI (User Interface) and backend.
- The UI uses VueJS, Vue Bootstrap, Vuex, and other libraries.
- The backend is in PHP 7.1, over Laravel 5.4 

The both are running on the same container. You can see the UI visiting the address [here](http://stream-viewer.tk:8888) .


### Database

The database container have a MySQL instance, and after migration on App (step after initial setup, to create the tables and seed database), have the follow tables:

- users - I've used the same structure from user table from Laravel, but have added a couple columns to hold the OAuth and profile data;
- videos - A list of videos. Currently have only 2.
- messages - The history of messages on chats
- migrations - internal setup table
- password_resets - is not used

### Chat

At beginning I've thinking in use the Pusher service, but I conclude that I could lose control over certain features I could want implement, and I've decided use a component I was working on, and adapt to this project, creating my own chat server. 

When a new connection arrives, there are a handshake message to setup the connection on server.
After that, all messages sent by an user are stored on database, and propagated to other users. 

## Notes

I'm not a expert on VueJS, and I spent a good time researching some solutions to problems I found, specially regarding the chat communication.

There are a lot of space for improvements of this small project. I've started to implement the structure for an admin panel, but when I've noted that could spent so much time with superfluous, 
I gave up and start to focus on initial requirements. 
