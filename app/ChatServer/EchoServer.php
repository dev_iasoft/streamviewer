<?php

namespace App\ChatServer;

use App\Message;

require_once(__DIR__.'/WebSocketServer.php');

class EchoServer extends WebSocketServer {

    function __construct($addr, $port, $bufferLength=1048576) {
      parent::__construct($addr, $port, $bufferLength);
      $this->userClass = '\\App\\ChatServer\\MyUser';
    }

    //protected $maxBufferSize = 1048576; //1MB... overkill for an echo server, but potentially plausible for other applications.

    protected function process ($user, $message) {

      if (substr($message,0,11) == '#handshake#') {
          $json = substr($message,12);
          $this->stdout("Handshake $json");

          $obj = json_decode($json,true);

        $userID = $user->id;
        $myID = $obj['user_id'];
        $this->users[$userID]->myUsername = $obj['username'];
        $this->users[$userID]->myChannel = $obj['channel'];
        $this->users[$userID]->myId = $myID;
        $this->stdout("Connection changing from $userID to $myID / channel=".$this->users[$userID]->myChannel);
        return;
      }

        $kUser = $user->id;
        $userID = $user->myId;
        $displayName =  $user->myDisplayName ?? $user->myUsername;
//        $this->stdout("Message from $userID: ");
//        $this->stdout(print_r($this->users,true));
//        $this->stdout(print_r($user,true));

        $channel = $this->users[$kUser]->myChannel;

//        $this->stdout("Contents: $message ");

        $this->saveMessage($userID, $channel, $message);

      foreach ($this->users as $userID => $connection) {
        if ($userID == $user->id) {
          //$this->send($connection,$message);

        } else {
          $this->send($connection,"{$displayName}: $message");
        }
      }

  //    $this->send($user,$message);
    }


    protected function saveMessage($userID, $channel, $contents) {

        try {
            $msg = new Message();
            $msg->user_id = $userID;
            $msg->video_id = $channel;
            $msg->contents = $contents;
            $msg->save();
        } catch (\Exception $e) {
            $this->stdout("Exception trying to save message from user $userID / channel=$channel: " . $e->getMessage());
        }

    }


    protected function connected ($user) {
      // Do nothing: This is just an echo server, there's no need to track the user.
      // However, if we did care about the users, we would probably have a cookie to
      // parse at this step, would be looking them up in permanent storage, etc.
    }

    protected function closed ($user) {
      // Do nothing: This is where cleanup would go, in case the user had any sort of
      // open files or other objects associated with them.  This runs after the socket
      // has been closed, so there is no need to clean up the socket itself here.
    }
  }
