<?php

namespace App\Http\Controllers;

use App\Message;
use App\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }


    public function getVideosList() {

        $vid = new Video();

        $resp = $vid->all();

        return $resp;
    }

    public function getVideo($id) {
        return [
            'id'=> $id
        ];
    }


    public function getSession() {
        $arr = session('user');
        $arr['is_logged'] = session('user_id');

        return $arr;
    }


    public function getChatMessages($id) {
        $resp = [];

        $sql = "SELECT m.*, u.email, u.name FROM messages m, users u WHERE m.video_id=? AND m.user_id=u.id";

        $ls = DB::select($sql,[$id]);

        foreach ($ls as $record) {
            $resp[] = [
                'when' => $record->created_at,
                'author' => $record->email,
                'contents' => $record->contents
            ];
        }


        return $resp;
    }


    public function getFansStats($id) {
        $resp = [];

        $sql = "SELECT m.*, u.email, u.name, u.id as user_id FROM messages m, users u WHERE m.video_id=? AND m.user_id=u.id ORDER BY m.created_at desc";

        $ls = DB::select($sql,[$id]);

        $fans = [];

        foreach ($ls as $record) {

            $userID = $record->user_id;

            if (!isset($fans[$userID])) {
                $fans[$userID] = [
                    'lastAccessed' => $record->created_at,
                    'messageCount' => 0,
                    'displayName' => $record->email
                ];
            }

            $fans[$userID]['messageCount']++;

        }

        sort($fans);

        return $fans;
    }
}
