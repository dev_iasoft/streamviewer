<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Redirect the user to the Google authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider()
    {

        $scopes = [
            'profile',
            'email'
        ];
        $response = Socialite::driver('google')->scopes($scopes)->redirect();
        Log::debug("redirect response = ".print_r($response,true));
        $r = $response->send();
        Log::debug("response2 = ".print_r($r,true));
        return $response;
    }

    /**
     * Obtain the user information from Google.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback()
    {
        try {
            $user = Socialite::driver('google')->user();
        } catch (\Exception $e) {
            Log::debug('Error =' . $e->getMessage());
            Log::debug('Trace =' . $e->getTraceAsString());
            return redirect('/');
        }

        // check if they're an existing user
        $existingUser = User::where('email', $user->email)->first();

        if($existingUser){
            // log them in
            auth()->login($existingUser, true);
        } else {
            // create a new user
            $newUser                  = new User();
            $newUser->name            = $user->name;
            $newUser->email           = $user->email;
            $newUser->google_id       = $user->id;
            $newUser->avatar          = $user->avatar;
            $newUser->avatar_original = $user->avatar_original;
            $newUser->role            = 1;
            try {
                $newUser->save();
            } catch (\Exception $e) {
                Log::debug('Error......'.print_r($e,true));
            }



            auth()->login($newUser, true);
        }
        Log::debug(print_r(auth()->user(),true));

        $myUser = [];
        $myUser['user'] = (array)$user;
        $myUser['type'] = gettype($user);
        $myUser['id'] = auth()->user()->id;
        $myUser['username'] = auth()->user()->email;
        $myUser['displayName'] = auth()->user()->name;
        //$myUser['type'] = gettype($user);




        session(['user'=>$myUser]);
        session(['user_id'=>auth()->user()->id]);

        Session::save();

        return redirect()->to('/');
    }
}
