<?php

function getServerAddr() {


    if (isset($_SERVER['HTTP_ORIGIN'])) {
        $httpOrigin = $_SERVER['HTTP_ORIGIN'];
//    } else
//        if (isset($_SERVER['SERVER_ADDR']) && isset($_SERVER['SERVER_PORT'])) {
//        $httpOrigin  = 'http://'.$_SERVER['SERVER_ADDR'].':'.$_SERVER['SERVER_PORT'];
    } else {
        $httpOrigin = env('APP_URL');
    }

//    $httpOrigin = getHostByName(getHostName());

    return $httpOrigin;
}


header("Access-Control-Allow-Origin: ".getServerAddr());



Route::get('/redirect', 'Auth\LoginController@redirectToProvider');
Route::get('/callback', 'Auth\LoginController@handleProviderCallback');



// --- Restore!!!
//Auth::routes();
// ----

//Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', function() {
    return view('welcome');
})->name('home');


Route::get('/logout', function() {
//    \Illuminate\Support\Facades\Auth::logout();
    session(['user'=>[]]);
    session(['user_id'=>0]);
    \Illuminate\Support\Facades\Session::save();
    return redirect('/');
});




Route::get('/session', '\\App\\Http\\Controllers\\HomeController@getSession');




Route::get('/js/config.json', function () {

    return [
        "myServerURL" => env('WS_SERVER'),
        "backendServer" => getServerAddr(),
        "googleAuthURL" => "http://www.google.com"
    ];

});



Route::get('/videos', '\\App\\Http\\Controllers\\HomeController@getVideosList');
Route::get('/video/{id}', '\\App\\Http\\Controllers\\HomeController@getVideo');
Route::get('/chat/{videoID}', '\\App\\Http\\Controllers\\HomeController@getChatMessages');
Route::get('/fans/{videoID}', '\\App\\Http\\Controllers\\HomeController@getFansStats');







Route::get('/{any?}', function (){
    return view('welcome');
})->where('any', '^(?!api\/)[\/\w\.-]*');
