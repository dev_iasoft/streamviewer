#!/bin/sh

cd /srv/app

## composer update

echo "waiting for 3 seconds to proceed (DB should be ready)..."

echo "3..."
sleep 1

echo "2..."
sleep 1

echo "1..."
sleep 1


echo "Migrating and seeding..."
php artisan migrate
