import Vue from 'vue'
import Vuex from 'vuex'
import ui from './modules/ui'
import video from './modules/video'
import auth from './modules/auth'
import chat from './modules/chat'
import makeConfig from './modules/config'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export function makeStore(config ) {

    var myConfig = makeConfig(config);

    return new Vuex.Store({

        state: {
            messagesList: new Array(),
            //config : config
        },
        mutations: {
            addMessage (obj) {
                // mutate state
                this.state.messagesList.push(obj)
            }
        },
        modules: {
            ui, video, auth, chat, config:myConfig
        },
        strict: debug

    })


};
