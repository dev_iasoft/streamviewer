import Vue from 'vue';
//import VueResource from 'vue-resource';

//Vue.use(VueResource);

const axios = require('axios');

Vue.prototype.$http = axios;

var myVue = new Vue();


let videos = [
    {
        id: 1,
        title: "18-core iMac Pro Review: Not a Trap!",
        thumbnail:
            "https://i.ytimg.com/vi/jn9mHzXJIV0/hqdefault.jpg?sqp=-oaymwEZCNACELwBSFXyq4qpAwsIARUAAIhCGAFwAQ==&rs=AOn4CLAvJvk4k_UNB9nst4pFP-txM1TLZA",
        youtubeURL: "https://www.youtube.com/embed/jn9mHzXJIV0",
        creator: "Marques Brownlee",
        likes: 0,
        views: 0
    },

];



const state = {
    currentView: false,
    videos:[],
    activeVideo : videos[0],
    messagesList : [],
    fansStats : []
}

const getters = {
    selected: state => state.currentView,
    active : state => state.activeVideo
}

// actions
const actions = {
    chooseVideo (context, video) {
        context.commit('selectVideo',video);

        var url = context.rootState.config.backendServer;
        // loadChatMessages

        myVue.$http.get(url+"/chat/"+video.id).then(response => {
            console.log(response.data);


            context.commit('updateMessageList',
                response.data
            );
        }, response => {
            console.log('error in updateMessageList!!!');
            console.log(response);
        });

        myVue.$http.get(url+"/fans/"+video.id).then(response => {
            console.log(response.data);


            context.commit('updateFansStats',
                response.data
            );
        }, response => {
            console.log('error in updateFansStats!!!');
            console.log(response);
        });

    },
    likeVideo (context) {
        context.commit('likeVideo');
    },
    loadVideos(context,url) {
        // console.log('@@ loadVideos');
        // console.log(url+"/videos");
        // console.log(myVue);

        myVue.$http.get(url+"/videos").then(response => {
            console.log(response.data);


            context.commit('updateVideosList',
                response.data
            );
        }, response => {
            console.log('error!!!');
            console.log(response);
        });
    }
}

const mutations = {
    ['selectVideo'] (state, video) {
        video.views++;
        state.activeVideo = video;
    },
    ['likeVideo'] (state) {
        state.activeVideo.likes++;
    },
    updateVideosList(state,list) {
        state.videos = list;
        state.activeVideo = list[0];
    },
    updateMessageList(state,list) {
        state.messagesList = list;
    },
    updateFansStats(state,list) {
        state.fansStats = list;
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}
