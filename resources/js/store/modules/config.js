import axios from "axios";

const state = {
    backendServer : ''
}


const mutations = {
    updateConfig(state, payload) {
        state.backendServer = payload.backendServer
    }
}

const actions = {

    requestConfig(context) {
        axios.get("/js/config.json").then(response => {
            context.dispatch('requestUpdateConfig',
                response.data
            );

        }, response => {
            console.log('error in requestConfig!!!');
            console.log(response);
        });
    },

    requestUpdateConfig(context, response) {
        context.commit('updateConfig', response);
    }

}



export default function (config) {

    console.log("### Building config");
    console.log(config);

    return {
        state : config
    }

}
