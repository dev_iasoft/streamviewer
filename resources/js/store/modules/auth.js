import axios from 'axios'
//import streamViewerApp from '../../app.js'
import store from '../index.js'




const state = {
    username:'',
    displayName: '',
    auth : false,
    is_logged: false,
    session:''
};

const mutations = {
    updateSessionData(state, payload) {
        console.log("##### ok... ");
        console.log(payload);
        state.user_id = payload.is_logged;
        state.is_logged = payload.is_logged;
        state.session = payload.session;
        state.username = payload.username;
        state.displayName = payload.displayName;
        state.auth = payload.auth;

    }
}


const actions = {
    requestSessionData(context, response) {
        context.commit('updateSessionData', response);
    },

    requestUpdateSessionData(context,response) {

        let cfg = context.rootState.config;

        axios.get(cfg.backendServer+"/session").then(response => {
            context.dispatch('requestSessionData',
                response.data
            );

        }, response => {
            console.log('error!!!');
            console.log(response);
        });
    }

};


export default {
    state,
    mutations,
    actions
}
