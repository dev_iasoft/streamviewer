import 'es6-promise/auto'
import axios from 'axios'
import './bootstrap'
import Vue from 'vue'
import VueAuth from '@websanova/vue-auth'
import VueAxios from 'vue-axios'
import VueRouter from 'vue-router'
import Index from './Index'
import auth from './auth'
import router from './router'
import BootstrapVue from 'bootstrap-vue'

const fetch = require("node-fetch");


Vue.use(BootstrapVue);

var streamViewerApp;

// Set Vue globally
window.Vue = Vue;

// Set Vue router
Vue.router = router;
Vue.use(VueRouter);

// Set Vue authentication
Vue.use(VueAxios, axios);
axios.defaults.baseURL = `${process.env.MIX_APP_URL}/api`;

//Vue.prototype.$http = axios;

Vue.use(VueAuth, auth);

// Load Index
Vue.component('index', Index);

const isDebug = process.env.NODE_ENV !== 'production';

var mainConfig=[];

import { makeStore } from './store/index.js'


console.log("is debug? "+isDebug);

// if (!isDebug) {
//     mainConfig = require('./config.json');
// } else {
    // /js/config.json

    const requestConfig = async () => {


        var host = window.location.origin;

        console.log('host = '+host);

        const response = await fetch(host+'/js/config.json')
        const json = await response.json();
        console.log("async/await based");
        console.log(json);
        mainConfig = json;

        main(mainConfig);
    }

    requestConfig();




// }

export function main(  mainConfig ) {

    console.log("Starting main with config: ");
    console.log(mainConfig);

    const myStore = makeStore(mainConfig);


    streamViewerApp = new Vue({
        el: '#app',
        store:myStore,
        router,
        render: h => h(Index)
    });

}

//main(mainConfig);

export default {
    streamViewerApp
};
