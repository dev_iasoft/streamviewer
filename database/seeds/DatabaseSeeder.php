<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        \App\User::create([
            'name' => 'Natalie',
            'email' => 'natalie@streamer.com',
            'password' => \Illuminate\Support\Facades\Hash::make('natalie'),
            'role' => 2
        ]);

        \App\User::create([
            'id' => 2,
            'name' => 'Tim Janis',
            'email' => 'kevin@super-fan.com',
            'password' => \Illuminate\Support\Facades\Hash::make('tim janis'),
            'role' => 2
        ]);


        \App\Video::create([
            'streamer_id' => 2,
            'title' => '2 hours of Instrumental Christmas Music with Fireplace "Warmest Christmas" by Tim Janis',
            'thumbnail' => 'https://i3.ytimg.com/vi/4gzqpfzxf94/hqdefault.jpg',
            'youtubeURL' => 'https://www.youtube.com/embed/4gzqpfzxf94',
            'creator' => 'Tim Janis'
        ]);

        // http://i3.ytimg.com/vi/Uk-ehJcmBEE/maxresdefault.jpg
        \App\Video::create([
            'streamer_id' => 2,
            'title' => 'Peaceful music, Relaxing music, Instrumental music "Echoes of Time" by Tim Janis',
            'thumbnail' => 'https://i3.ytimg.com/vi/Uk-ehJcmBEE/hqdefault.jpg',
            'youtubeURL' => 'https://www.youtube.com/embed/Uk-ehJcmBEE',
            'creator' => 'Tim Janis'
        ]);

//        \App\Video::create([
//            'title' => '',
//            'thumbnail' => '',
//            'youtubeURL' => '',
//            'creator' => ''
//        ]);

    }
}
